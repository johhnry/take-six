```shell
$ haxelib newrepo # Setup the local repository once
$ haxelib install all --always # Install all the libraries in .hxml files
$ haxe build.hxml # Build the code for multiple targets
$ haxe test.hxml # Run the tests
```
