package test;

import utest.Runner;
import utest.ui.Report;

class TestsMain {
	public static function main() {
		var runner = new Runner();
		runner.addCases("test.cases");

		Report.create(runner);
		runner.run();
	}
}
