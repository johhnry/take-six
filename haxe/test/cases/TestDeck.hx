package test.cases;

import utest.Assert;

class TestDeck extends utest.Test {
	var deck:Deck;

	public function setup() {
		deck = new Deck();
	}

	public function testDeckDraw() {
		var lengthBefore = deck.size();
		deck.draw();
		Assert.equals(deck.size(), lengthBefore - 1);
	}
}
