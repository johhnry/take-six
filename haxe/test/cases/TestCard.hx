package test.cases;

import utest.Assert;

class TestCard extends utest.Test {
	public function testCardPoints() {
		Assert.equals(new Card(55).points, 7);
		Assert.equals(new Card(11).points, 5);
		Assert.equals(new Card(85).points, 2);
		Assert.equals(new Card(104).points, 1);
	}
}
