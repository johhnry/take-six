typedef Stack = Array<Card>;

class Game {
	static final NSTACKS = 4;
	static final NCARDS = 10;

	var deck:Deck;
	var stacks:Array<Stack>;
	var players:Array<Player>;

	public function new(options:{nPlayers:Int}) {
		deck = new Deck();
		stacks = [for (_ in 0...NSTACKS) []];
		players = [for (_ in 0...options.nPlayers) new Player()];
	}

	function distributeCards() {
		for (_ in 0...NCARDS) {
			for (player in players) {
				player.drawFrom(deck);
			}
		}
	}

	function placeCardOnStacks() {
		for (stack in stacks) {
			stack.push(deck.draw());
		}
	}

	public function setup() {
		distributeCards();
		placeCardOnStacks();
	}
}
