final class Card {
	public var number:Int;
	public var points:Int;

	static function pointsFromNumber(number) {
		var points = 0;

		if (number % 5 == 0)
			points += 2;
		if (number % 10 == 0)
			points += 1;
		if (number % 11 == 0)
			points += 5;

		return (points != 0) ? points : 1;
	}

	public function new(number) {
		this.number = number;
		points = Card.pointsFromNumber(number);
	}
}
