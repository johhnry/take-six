import Random;

final class Deck {
	static final NCARDS = 104;

	public var cards:Array<Card>;

	public function new() {
		cards = [for (i in 1...NCARDS + 1) new Card(i)];
		Random.shuffle(cards);
	}

	public function draw() {
		return cards.pop();
	}

	public function size() {
		return cards.length;
	}
}
