final class Player {
	var hand:Array<Card>;

	public function new() {
		hand = [];
	}

	public function drawFrom(deck:Deck) {
		hand.push(deck.draw());
	}
}
