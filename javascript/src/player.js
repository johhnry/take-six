class Player {
  constructor() {
    this.hand = [];
  }

  /**
   * Draws a card from a deck and add it to the player's hand
   * @param {Deck} deck
   */
  drawFrom(deck) {
    this.hand.push(deck.draw());
  }
}

export default Player;
