import Card from "./card.js";
import { shuffleArray } from "./utils.js";

const MAX_CARD_NUMBER = 104;

class Deck {
  constructor() {
    this.cards = [];

    for (let i = 1; i <= MAX_CARD_NUMBER; i++) {
      this.cards.push(new Card(i));
    }

    shuffleArray(this.cards);
  }

  /**
   * Draws a card from the deck
   * @returns the last card on the top
   */
  draw() {
    return this.cards.pop();
  }
}

export default Deck;
