import Deck from "./deck.js";
import Player from "./player.js";

const NSTACKS = 4;
const NCARDS = 10;

class Game {
  constructor(options) {
    this.deck = new Deck();
    this.players = [];

    // Create the players
    for (let i = 0; i < options.nPlayers; i++) {
      this.players.push(new Player());
    }

    // Initialize the stacks
    this.stacks = [];
    for (let i = 0; i < NSTACKS; i++) {
      this.stacks[i] = [];
    }
  }

  setup() {
    // Distribute cards to the players
    for (let i = 0; i < NCARDS; i++) {
      for (const player of this.players) {
        player.drawFrom(this.deck);
      }
    }

    // Distribute cards on the stacks
    for (const stack of this.stacks) {
      stack.push(this.deck.draw());
    }
  }
}

export default Game;
