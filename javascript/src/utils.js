/**
 * Randomize array in-place using Durstenfeld shuffle algorithm
 * See: https://stackoverflow.com/a/12646864
 * @param {Array} array
 * @returns {Array}
 */
export function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
}
