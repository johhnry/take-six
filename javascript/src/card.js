class Card {
  static getPointsFromNumber(number) {
    let points = 0;
    if (number % 5 === 0) points += 2;
    if (number % 10 === 0) points += 1;
    if (number % 11 === 0) points += 5;
    return points === 0 ? 1 : points;
  }

  constructor(number) {
    this.number = number;
    this.points = Card.getPointsFromNumber(number);
  }
}

export default Card;
